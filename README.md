# LuckSheet_.NetCore

#### 介绍
使用.net core 3.1和Npoi 制作基于LuckSheet的基础导出
欢迎大家完善修补
#### 使用说明

1.  上传是必须拥有config和celldata
2.  上传是内容以JSON格式上传
3.  前端加密使用的是pako，下面是加密解密方法
        ///GZIP解密
        function unzip(b64Data) {
            var strData = atob(b64Data);
            var charData = strData.split('').map(function (x) { return x.charCodeAt(0); });
            var binData = new Uint8Array(charData);
            var data = pako.inflate(binData);
            strData = String.fromCharCode.apply(null, new Uint16Array(data));
            return decodeURIComponent(strData);
        }

        ///GZIP加密
        function zip(str) {
            var binaryString = pako.gzip(encodeURIComponent(str), { to: 'string' })
            return btoa(binaryString);
        }

#### 参与贡献



#### 特技
