﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LuckSheet_.NetCore.BizModels.Request
{
    public class ExcelDataRequest
    {
        /// <summary>
        /// 数据
        /// </summary>
        public string Data { get; set; }
    }
}
